require 'schedule'

describe Schedule, 'class' do 
  context 'morning range' do
    it 'must to start at 9' do
      expect(Schedule.ranges[:'morning'].first).to eq(9)
    end

    it 'must to ends at 12' do
      expect(Schedule.ranges[:'morning'].last).to eq(12)
    end
  end

  context 'afternoon range' do
    it 'must to start at 13' do
      expect(Schedule.ranges[:'afternoon'].first).to eq(13)
    end

    it 'must to ends at 17' do
      expect(Schedule.ranges[:'afternoon'].last).to eq(17)
    end
  end
end