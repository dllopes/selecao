require 'conference'

describe Conference, 'class' do 
  context 'tracks' do
    it 'must be 2 valid tracks' do
      conference = Conference.new
      expect(conference.tracks.select{|t| t.valid?}.size).to eq(2)
    end
  end
end 