class ReadFile
   attr_reader :data, :scanned, :file_name

   def initialize(file_name='proposals.txt')
     @file_name = file_name
     process
     scan
   end

   def process
     @data = File.read(@file_name)
   end

   def scan
     @scanned = []
     @data.scan(/(.+?(?= ([0-9]+)min| lightning))/) {|x,y| @scanned << [x, y ? y.to_i : 5]}
     @scanned
   end
end