require_relative 'session'
require_relative 'read_file'

class Schedule
    @@ranges ={
        'morning': 9..12,
        'noon': 12..13,
        'afternoon': 13..17
    }

    def self.ranges
        @@ranges
    end

    def self.create_sessions
        scanned_data = ReadFile.new.scan
        @sessions = []
        while !scanned_data.empty? do
            ['morning','afternoon'].each do |period|
                session = Session.new(period, scanned_data)
                scanned_data = scanned_data - session.speeches
                @sessions << session.speeches      
            end
        end    
        @sessions
    end
end