require_relative 'schedule'

class Session
    attr_accessor :range, :period, :speeches, :scanned_data

    def initialize(period, scanned_data)
      @period = period
      @scanned_data = scanned_data
      @range = Schedule.ranges[period.to_sym]
      add_speeches
    end

    private
    def add_speeches
      minutes = range_to_minutes
      combinations = []
      letters = ('a'..'g').to_a
      sizes = ((period == 'morning' ? (3..4) : (4..letters.size)).to_a.reverse)
      sizes.each do |size|
        all_items = "#{letters[0..(size-1)].join(',')}"
        sum_all_items = "#{letters[0..(size-1)].join('[1]+')}[1]"
        combinations = eval("@scanned_data.combination(#{size}).select{ |#{all_items}| #{sum_all_items} == #{minutes} }") unless combinations.any?
      end
      @speeches = combinations.empty? ? @scanned_data : combinations.first
    end

    def range_to_minutes
      (@range.last - @range.first)*60
    end
end