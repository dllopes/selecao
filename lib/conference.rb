require_relative 'schedule'
require_relative 'track'
require 'active_support/time'

class Conference
    attr_reader :sessions, :sessions_temp, :tracks

    def initialize
        @tracks = []
        @sessions = Schedule.create_sessions
        @sessions_temp = @sessions
        ('A'..'Z').to_a[0..(sessions.size/2)-1].each_with_index do |track_name, index|
            @tracks << Track.new(track_name, @sessions_temp[0], @sessions_temp[1])
            @sessions_temp -= @sessions_temp[0..1]
        end
    end
    
    def print_schedule
        @tracks.select{|t| t.valid?}.each_with_index do |track|
            puts "TRACK #{track.title}"
            start_at = Time.now.change({hour: 9})
            start_at = track.print_sessions('morning', start_at)
            puts "#{start_at.strftime('%H:%M')}  ALMOÇO"
            start_at += 60.minutes
            start_at = track.print_sessions('afternoon', start_at)
            puts "#{start_at.strftime('%H:%M')}  Evento de Networking"
            puts ''
        end

        unless @tracks.select{|t| !t.valid?}.empty?
          puts 'Ainda há palestras, porém não é possível organizá-las em um novo TRACK devido restrições'
        end
    end

    private
    def print_line(lecture, start_at)
        time = lecture[1] == 5 ? 'lighting' : "#{lecture[1]}min"
        puts "#{start_at.strftime('%H:%M')}  #{lecture[0]} #{time}" 
        lecture[1].minutes
    end
end