class Track
  attr_reader :title, :morning_session, :afternoon_session

  def initialize(title, morning_session, afternoon_session)
    @title = title
    @morning_session = morning_session || ['',0]
    @afternoon_session = afternoon_session || ['',0]
  end

  def print_sessions(period, start_at)
    sessions = (period == 'morning' ? @morning_session : @afternoon_session)
    sessions.each do |lecture|
      time = lecture[1] == 5 ? 'lighting' : "#{lecture[1]}min"
      puts "#{start_at.strftime('%H:%M')}  #{lecture[0]} #{time}"
      start_at += lecture[1].minutes
    end
    start_at
  end

  def valid?
    afternoon_sum = afternoon_session.sum{|item| item[1]}
    (afternoon_sum >= 180 && afternoon_sum <= 240) && !morning_session.empty? && !afternoon_session.empty?
  end
end
